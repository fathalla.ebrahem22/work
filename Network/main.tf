resource "aws_vpc" "eks_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    "Name" = "${var.prefix_env}-eks-vpc"
  }
}

resource "aws_subnet" "eks_public_subnet_1" {
  vpc_id            = aws_vpc.eks_vpc.id
  cidr_block        = var.sub_1
  availability_zone = var.avail_zone_1
  map_public_ip_on_launch = true
  tags = {
    "Name" = "${var.prefix_env}_public_subnet_1"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/demo_eks" = "owned"
  }
}

resource "aws_subnet" "eks_public_subnet_2" {
  vpc_id            = aws_vpc.eks_vpc.id
  cidr_block        = var.sub_2
  availability_zone = var.avail_zone_2
  map_public_ip_on_launch = true
  tags = {
    "Name" = "${var.prefix_env}_public_subnet_2"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/demo_eks" = "owned"
  }
}

resource "aws_subnet" "eks_private_subnet_1" {
  vpc_id            = aws_vpc.eks_vpc.id
  cidr_block        = var.sub_3
  availability_zone = var.avail_zone_1
  tags = {
    "Name" = "${var.prefix_env}_private_subnet_1"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/demo_eks" = "owned"
  }
}

resource "aws_subnet" "eks_private_subnet_2" {
  vpc_id            = aws_vpc.eks_vpc.id
  cidr_block        = var.sub_4
  availability_zone = var.avail_zone_2
  tags = {
    "Name" = "${var.prefix_env}_private_subnet_2"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/demo_eks" = "owned"
  }
}


resource "aws_internet_gateway" "eks_IGW" {
  vpc_id = aws_vpc.eks_vpc.id
  tags = {
    "Name" = "${var.prefix_env}_eks_IGW"
  }
}

resource "aws_route_table" "public_Route_Table" {
  vpc_id = aws_vpc.eks_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.eks_IGW.id
  
     # destination_prefix_list_id = ""
     # egress_only_gateway_id     = ""
     # instance_id                = ""
     # local_gateway_id           = ""
      #network_interface_id       = ""
     # transit_gateway_id         = ""
      #vpc_endpoint_id            = ""
      #vpc_peering_connection_id  = ""
  }
  tags = {
    "Name" = "${var.prefix_env}_Public__RTP"
  }
}

resource "aws_route_table" "private_Route_Table" {
   
  vpc_id = aws_vpc.eks_vpc.id
   
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_eks.id
   # carrier_gateway_id         = ""
   #   destination_prefix_list_id = ""
    #  egress_only_gateway_id     = ""
    #  instance_id                = ""
    #  local_gateway_id           = ""
    #  network_interface_id       = ""
   #   transit_gateway_id         = ""
    #  vpc_endpoint_id            = ""
    #  vpc_peering_connection_id  = ""
  }
  tags = {
    "Name" = "${var.prefix_env}_Private__RTP"
  }

}
resource "aws_eip" "lb" {
  vpc      = true
  tags = {
    Name = "eip-eks"
  }
}
resource "aws_nat_gateway" "nat_eks" {
  allocation_id = aws_eip.lb.id
  subnet_id     = aws_subnet.eks_public_subnet_1.id

  tags = {
    Name = "GW NAT-eks"
  }

  
}

resource "aws_route_table_association" "Associate_sub_1_Public_RTP" {
  subnet_id      = aws_subnet.eks_public_subnet_1.id
  route_table_id = aws_route_table.public_Route_Table.id
}

resource "aws_route_table_association" "Associate_sub_2_Public_RTP" {
  subnet_id      = aws_subnet.eks_public_subnet_2.id
  route_table_id = aws_route_table.public_Route_Table.id
}

resource "aws_route_table_association" "Associate_sub_3_Private_RTP" {
  subnet_id      = aws_subnet.eks_private_subnet_1.id
  route_table_id = aws_route_table.private_Route_Table.id
}

resource "aws_route_table_association" "Associate_sub_4_Private_RTP" {
  subnet_id      = aws_subnet.eks_private_subnet_2.id
  route_table_id = aws_route_table.private_Route_Table.id
}