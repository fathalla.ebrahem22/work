output "vpc_eks_id" {
  value = aws_vpc.eks_vpc.id
}

output "sub_1_public_id" {
  value = aws_subnet.eks_public_subnet_1.id
}
output "sub_2_public_id" {
  value = aws_subnet.eks_public_subnet_2.id
}
output "sub_1_private_id" {
  value = aws_subnet.eks_private_subnet_1.id
}
output "sub_2_private_id" {
  value = aws_subnet.eks_public_subnet_2.id
}

