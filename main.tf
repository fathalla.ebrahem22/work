provider "aws" {
  region  = var.region
}

terraform {
 backend "s3" {
   bucket         = "tfstate2050"
   key            = "state/terraform.tfstate"
   region         = "us-east-1"
   dynamodb_table = "terraform-state"
 }
}

module "network" {
  source       = "./Network"
  vpc_cidr     = var.vpc_cidr
  sub_1        = var.sub_1
  sub_2        = var.sub_2
  sub_3        = var.sub_3
  sub_4        = var.sub_4
  avail_zone_1 = var.avail_zone_1
  avail_zone_2 = var.avail_zone_2
  prefix_env   = var.prefix_env
}

module "eks" {
  source = "./EKS"
  eks_private_subnet_1 = module.network.sub_1_public_id
  eks_private_subnet_2 = module.network.sub_2_public_id
  eks_public_subnet_1 = module.network.sub_1_private_id
  eks_public_subnet_2 = module.network.sub_2_private_id
}

